/*
 * Crystall.cpp
 *
 *  Created on: 04 ���. 2014 �.
 *      Author: Vladimir
 */

#include "Crystall.h"

#include <SDL_render.h>
#include <SDL_stdinc.h>
#include <cmath>
#include <cstdlib>

#define axp(a,b,c,d) this->axispoints[a] = {b, c, d};
#define crp(a,b,c,d) this->crystallpoints[a] = {b, c, d};
const int correctToCenter = 40;
Crystall* Crystall::crystall;
enum axis{c, b, k};

Crystall::Crystall(Window::windowenum w) {
	axp(0, 0, 0, 0)
	axp(1, 1, 0, 0)//--x
	axp(2, 0, 1, 0)//--y
	axp(3, 0, 0, 1)//--z
	crp(0, 0, 0, 0)
	crp(1, 30,  0, 0)//--x
	crp(2, 30, 30, 0)
	crp(3,  0, 30, 0)//--y
	crp(4,  0,  0, 60)//--z
	crp(5, 30,  0, 60)
	crp(6, 30, 30, 60)
	crp(7,  0, 30, 60)
	this->cwindow = w;
}
void Crystall::reset(){
	axp(0, 0, 0, 0)
	axp(1, 1, 0, 0)//--x
	axp(2, 0, 1, 0)//--y
	axp(3, 0, 0, 1)//--z
	crp(0, 0, 0, 0)
	crp(1, 30,  0, 0)//--x
	crp(2, 30, 30, 0)
	crp(3,  0, 30, 0)//--y
	crp(4,  0,  0, 60)//--z
	crp(5, 30,  0, 60)
	crp(6, 30, 30, 60)
	crp(7,  0, 30, 60)
}
Crystall::~Crystall() {}
void Crystall::drawAxis(){
	Quaterion::point a = axispoints[2];
	a.y *= Window::windows[this->cwindow]->getHeight()/2 - correctToCenter;
	SDL_Point ii = Crystall::adduction(a);
	for(int i=-1;i<1;++i){
		drawlineFromCenter(Window::windows[this->cwindow]->getWidth(), Window::windows[this->cwindow]->getHeight()/2 - correctToCenter, i);
		drawlineFromCenter(Window::windows[this->cwindow]->getWidth()/2, 0, 0, i);
		drawlineFromCenter(ii.x, ii.y, i+1);
		drawlineFromCenter(ii.x, ii.y, i-1);
	}
}
void Crystall::drawlineFromCenter(int x, int y, int i, int j){
	SDL_RenderDrawLine(Window::windows[this->cwindow]->getRenderer(Window::normal), Window::windows[this->cwindow]->getWidth()/2+j, Window::windows[this->cwindow]->getHeight()/2 - correctToCenter + i, x + j, y + i);
}
void Crystall::drawline(SDL_Point p1, SDL_Point p2, int i, int j){
	SDL_RenderDrawLine(Window::windows[this->cwindow]->getRenderer(Window::normal), p1.x + j, p1.y + i, p2.x + j, p2.y + i);
}
void Crystall::drawCrystallLineSegment(Crystall::point2* p, int count){
	for(int i=0;i<count;++i){
		for(int j=0;j<p[i].s;++j){
			drawline(p[i].p, p[i+1].p, j, 0);
			//drawline(p[i].p, p[i+1].p, 0, j);
		}
	}
	//SDL_Rect rect = {p[0].p.x + p->s/4, p[0].p.y + p->s/4, p->s, p->s};
	//SDL_RenderFillRect(Window::windows[this->cwindow]->getRenderer(Window::normal), &rect);
}
void Crystall::drawCrystallLine(Quaterion::point ps, Quaterion::point pe, double maxy, double miny){
	double ll = (double)Window::windows[this->cwindow]->getHeight() / (double)(SCREEN_DIRECTION_HEIGHT);
	Quaterion::point p1, p2;
	Quaterion::point temppoint;
	p1.x = ps.x * ll;
	p1.y = ps.y * ll;
	p1.z = ps.z * ll;
	p2.x = pe.x * ll;
	p2.y = pe.y * ll;
	p2.z = pe.z * ll;
	maxy *= ll;
	miny *= ll;
	if(p1.y>p2.y){
		temppoint = p2;
		p2 = p1; p1 = temppoint;
	}
	ll = abs(p1.y - p2.y);
	int lengthofsegment = (maxy-miny)/6;
	int l = ceil(ll/lengthofsegment);
	if(l==0) l=1;
	Crystall::point2 arraypoints[l+1];
	SDL_Point sdlpoint = Crystall::adduction(p1);
	arraypoints[0] = {sdlpoint, (int)((p1.y - miny)*6/(maxy-miny))+1};
	for(int i=1;i<l;++i){
		double lambda = lengthofsegment * i / (p2.y-p1.y);
		temppoint.x = p1.x + (p2.x - p1.x)*lambda;
		temppoint.y = p1.y + lengthofsegment*i;
		temppoint.z = p1.z + (p2.z - p1.z)*lambda;
		sdlpoint = Crystall::adduction(temppoint);
		arraypoints[i] = {sdlpoint, (int)((temppoint.y - miny)*6/(maxy-miny))+1};
	}
	sdlpoint = Crystall::adduction(p2);
	arraypoints[l] = {sdlpoint, (int)((temppoint.y - miny)*6/(maxy-miny))+1};
	Crystall::drawCrystallLineSegment(arraypoints, l);

}
double Crystall::Max(double a, double b){
	return a>b ? a : b;
}
double Crystall::Min(double a, double b){
	return a<b ? a : b;
}
template <typename T>
void Crystall::sortPairArray(std::pair<int, T>* tmpy, int n){
	int i=0, j=n, p;
	p = tmpy[n/2].second;
	do{
		while(tmpy[i].second>p) ++i;
		while(tmpy[j].second<p) --j;
		if(i<=j){
			swap(tmpy[i], tmpy[j]);
			++i; --j;
		}
	}while(i<=j);
	if(j>0)sortPairArray(tmpy, j);
	if(n>i)sortPairArray(tmpy + i, n-i);
}
void Crystall::drawCrystall(){
	std::pair<int, int> tmpy[8];
	for(int i=7; i>=0;--i){
		tmpy[i] = std::make_pair(i, crystallpoints[i].y+0.5);
	}
	sortPairArray(tmpy, 7);
	double maxy = tmpy[1].second;
	double miny = tmpy[7].second;
	for(int i=7; i>=0;--i){
		switch(tmpy[i].first){
		case 1:drawCrystallLine(crystallpoints[1], crystallpoints[2], maxy, miny);
				drawCrystallLine(crystallpoints[1], crystallpoints[5], maxy, miny);break;
		case 2:drawCrystallLine(crystallpoints[2], crystallpoints[3], maxy, miny);
				drawCrystallLine(crystallpoints[2], crystallpoints[6], maxy, miny);break;
		case 3:drawCrystallLine(crystallpoints[3], crystallpoints[7], maxy, miny);break;
		case 4:drawCrystallLine(crystallpoints[4], crystallpoints[5], maxy, miny);break;
		case 5:drawCrystallLine(crystallpoints[5], crystallpoints[6], maxy, miny);break;
		case 6:drawCrystallLine(crystallpoints[6], crystallpoints[7], maxy, miny);break;
		case 7:drawCrystallLine(crystallpoints[7], crystallpoints[4], maxy, miny);break;
		case 0:
			SDL_SetRenderDrawColor(Window::windows[this->cwindow]->getRenderer(Window::normal), 227, 9, 158, 255);
			drawCrystallLine(crystallpoints[0], crystallpoints[1], maxy, miny);
			SDL_SetRenderDrawColor(Window::windows[this->cwindow]->getRenderer(Window::normal), 7, 130, 15, 255);
			drawCrystallLine(crystallpoints[3], crystallpoints[0], maxy, miny);
			SDL_SetRenderDrawColor(Window::windows[this->cwindow]->getRenderer(Window::normal), 7, 123, 130, 255);
			drawCrystallLine(crystallpoints[0], crystallpoints[4], maxy, miny);
			SDL_SetRenderDrawColor(Window::windows[this->cwindow]->getRenderer(Window::normal), 44, 6, 111, 255);
			break;
		}
	}
}
SDL_Point Crystall::adduction(Quaterion::point p){
	return {Window::windows[this->cwindow]->getWidth()/2 + (int)((p.x - cos(45) * p.y)), Window::windows[this->cwindow]->getHeight()/2 - correctToCenter - (int)((p.z - sin(45) * p.y))};
}
void Crystall::rotate(float anglek, float angleb, float anglec){
	Quaterion crystallpointsQuaterion[8];
	Quaterion rotation;
	for(int i=0; i<8; ++i){
		crystallpointsQuaterion[i] = Quaterion(crystallpoints[i]);
	}
	if(angleb != 0){
		rotation = Quaterion(crystallpoints[3], angleb);
		Crystall::rotateCrystall(&rotation, crystallpointsQuaterion, 8);
	}
	if(anglec != 0){
		rotation = Quaterion(crystallpoints[1], anglec);
		Crystall::rotateCrystall(&rotation, crystallpointsQuaterion, 8);
	}
	if(anglek != 0){
		rotation = Quaterion(crystallpoints[4], anglek);
		Crystall::rotateCrystall(&rotation, crystallpointsQuaterion, 8);
	}
	ImageSpector::imageSpector->rotateSpector(anglek, angleb, anglec);
}
void Crystall::rotateCrystall(Quaterion* rotation, Quaterion* q, int count){
	rotation->normalize();
	for(int i=0;i<count;++i){
		q[i].multiplication(rotation);
		q[i].toPoint(crystallpoints[i]);
	}
}
/*
void Crystall::Quaterion2point(Crystall::point* p, Crystall::Quaterion* q, Crystall::point* output){
	  float
	    xx = q->x * q->x, yy = q->y * q->y, zz = q->z * q->z,
	    xy = q->x * q->y, xz = q->x * q->z,
	    yz = q->y * q->z, wx = q->w * q->x,
	    wy = q->w * q->y, wz = q->w * q->z;

	  output->x =
	    (1.0f - 2.0f * ( yy + zz )) * p->x +
	    (2.0f * ( xy - wz )) * p->y  +
	    (2.0f * ( xz + wy )) * p->z;
	  output->y =
	    (2.0f * ( xy + wz )) * p->x +
	    (1.0f - 2.0f * ( xx + zz )) * p->y +
	    (2.0f * ( yz - wx )) *p->z;
	  output->z =
	    (2.0f * ( xz - wy )) * p->x +
	    (2.0f * ( yz + wx )) * p->y +
	    (1.0f - 2.0f * ( xx + yy )) * p->z;
}
*/
