/*
 * ImageSpector.cpp
 *
 *  Created on: 28 ���. 2014 �.
 *      Author: Vladimir
 */

#include "ImageSpector.h"

#include <SDL_render.h>
#include <SDL_video.h>
#include <cmath>
//#include <fstream>
//#include <iostream>
#include <iterator>
#include <sstream>
//#include <string>

#include "TextureText.h"

ImageSpector* ImageSpector::imageSpector;

ImageSpector::ImageSpector(Window::windowenum w): energyBegin(0), energyEnd(0) {
	vecB = {0, 1, 0};
	vecC = {0, 0, -1};
	vecK = {1, 0, 0};
	anglek = 0;
	angleb = 0;
	anglec = 0;
	line = 0;
	this->swindow = w;
}
ImageSpector::~ImageSpector() {
	// TODO Auto-generated destructor stub
}
void ImageSpector::readFile(std::string path){
	//Crystall::crystall->reset();
	if(spector.empty()){
		SDL_SetWindowTitle(Window::windows[swindow]->getSDLWindow(), path.c_str());
		Spector sp = Spector();
		if(!sp.readFile(path, energyBegin, energyEnd, this->line))
			return;
		this->spector.push_back(sp);
		energyBegin = sp.energy[0];
		energyEnd = sp.energy[line-1];
	}
	Spector sp = Spector();
	if(!sp.readFile(path, energyBegin, energyEnd, this->line))
		return;
	this->spector.push_back(sp);
	recalculate();
}
void ImageSpector::reset(){
	if(spector.empty())
		return;
	anglek = 0;
	angleb = 0;
	anglec = 0;
	std::string path = SDL_GetWindowTitle(Window::windows[swindow]->getSDLWindow());
	spector.clear();
	readFile(path);
}
double ImageSpector::Max(double a, double b){
	return a>b ? a : b;
}
double ImageSpector::Min(double a, double b){
	return a<b ? a : b;
}
void ImageSpector::render(){
	const int offset = 20;
		int wWin = Window::windows[swindow]->getWidth();
		int hWin = Window::windows[swindow]->getHeight() - offset;
		SDL_SetRenderDrawColor(Window::windows[swindow]->getRenderer(Window::normal), 255, 255, 255, 255);
		for (int i = 0; i < wWin; i += 30)
			SDL_RenderDrawPoint(Window::windows[swindow]->getRenderer(Window::normal), i, hWin);
		for (int i = 0; i < wWin; i += 30)
			SDL_RenderDrawPoint(Window::windows[swindow]->getRenderer(Window::normal), i, hWin/3);
		for (int i = 0; i < wWin; i += 30)
			SDL_RenderDrawPoint(Window::windows[swindow]->getRenderer(Window::normal), i, (hWin*2)/3);
		for (int i = 0; i < hWin; i += 30)
			SDL_RenderDrawPoint(Window::windows[swindow]->getRenderer(Window::normal), 0, i);
		for (int i = 0; i < hWin; i += 50)
			SDL_RenderDrawPoint(Window::windows[swindow]->getRenderer(Window::normal), wWin/3, i);
		for (int i = 0; i < hWin; i += 50)
			SDL_RenderDrawPoint(Window::windows[swindow]->getRenderer(Window::normal), (wWin*2)/3, i);
		for (int i = 0; i < hWin; i += 50)
			SDL_RenderDrawPoint(Window::windows[swindow]->getRenderer(Window::normal), wWin, i);
		std::vector<Spector>::iterator it = spector.begin();
		int shiftColor = 0;
		while(it<spector.end()){
			if(shiftColor > 19 )shiftColor = 0;
			(*it).render(wWin, hWin, Window::windows[swindow]->getRenderer(Window::normal), shiftColor, energyBegin);
			shiftColor+=2;
			++it;
		}
		TextureText::texturetexts[TextureText::ylable4]->renderIt(0, 0);
		TextureText::texturetexts[TextureText::ylable3]->renderIt(0, hWin/3);
		TextureText::texturetexts[TextureText::ylable2]->renderIt(0, (hWin*2)/3);
		TextureText::texturetexts[TextureText::ylable1]->renderIt(0, hWin);
		TextureText::texturetexts[TextureText::xlable4]->renderIt(wWin - TextureText::texturetexts[TextureText::xlable4]->getWidth(), hWin);
		TextureText::texturetexts[TextureText::xlable3]->renderIt((wWin*2)/3- TextureText::texturetexts[TextureText::xlable4]->getWidth(), hWin);
		TextureText::texturetexts[TextureText::xlable2]->renderIt(wWin/3- TextureText::texturetexts[TextureText::xlable4]->getWidth(), hWin);
}
void ImageSpector::print(){
	if(spector.empty())	return;
	std::string st("output\\");
	st.append(SDL_GetWindowTitle(Window::windows[swindow]->getSDLWindow()));
	unsigned int hh = st.rfind("\\");
	if(hh != st.size())
		st.erase(7, hh - 6);
	hh = st.rfind(".");
	if(hh != st.size())
		st.erase(hh);
	std::stringstream str;
	str << "_" << angleb << "_" << anglek << ".dat";
	st += str.str();
	std::ofstream outputfile(st);
	if(outputfile == NULL) return;
	for(int i = 0; i < spector[1].line; ++i){
		outputfile << spector[1].energy[i] << "          ";
		outputfile << spector[1].intensity[i][0] << "          ";
		outputfile << spector[1].intensity[i][1] << "          ";
		outputfile << std::endl;
	}
}
void ImageSpector::rotateSpector(float ak, float ab, float ac){
	if(spector.empty()) return;
	anglek += ak;
	angleb += ab;
	anglec += ac;
	Quaterion qk = Quaterion(vecK);
	Quaterion qb = Quaterion(vecB);
	Quaterion qc = Quaterion(vecC);
	Quaterion rotb = Quaterion(vecB, angleb);
	rotb.normalize();
	qk.multiplication(&rotb);
	qc.multiplication(&rotb);
	Quaterion::point p = {qk.x, qk.y, qk.z};
	Quaterion rotk = Quaterion(p, anglek);
	rotk.normalize();
	qc.w = 0;
	qc.multiplication(&rotk);
	qb.multiplication(&rotk);
	double mp_a, mp_b, mp_c;
	for(int i = 0; i < spector[1].line; ++i){
		if(spector[0].intensity[i][0] < 0)
			mp_b = 0;
		else mp_b = sqrt(spector[0].intensity[i][0]);
		if(spector[0].intensity[i][1] < 0){
			mp_a = 0; mp_c = 0;
		} else {
			mp_c = sqrt(spector[0].intensity[i][1]);
			mp_a = mp_c * 0.9163 / 0.4004;
		}
		spector[1].intensity[i][0] = qb.projected(mp_a, mp_b, mp_c);
		spector[1].intensity[i][1] = qc.projected(mp_a, mp_b, mp_c);
	}
	recalculate();
}
void ImageSpector::recalculate(){
	std::vector<Spector>::iterator it = spector.begin();
	while(it < spector.end()){
		(*it).recalculateMinMax(energyEnd - energyBegin);
		++it;
	}
	it = spector.begin();
	++it;
	int sI = ((*(it - 1)).scaleI);
	while(it < spector.end()){
		if((*it).scaleI > ((*(it - 1)).scaleI)){
			sI = (*it).scaleI;
			(*(it - 1)).setScaleI((*it).scaleI);
			it = spector.begin();
			++it;
			continue;
		}
		if((*it).scaleI < ((*(it - 1)).scaleI)){
			sI = (*(it - 1)).scaleI;
			(*it).setScaleI((*(it - 1)).scaleI);
			it = spector.begin();
			++it;
			continue;
		}
		++it;
	}
	std::stringstream s4;
	s4 << sI;
	TextureText::texturetexts[TextureText::ylable4]->loadFromRenderedText(s4.str());
	std::stringstream s3;
	s3 << (int)((sI*2)/3);
	TextureText::texturetexts[TextureText::ylable3]->loadFromRenderedText(s3.str());
	std::stringstream s2;
	s2 << (int)(sI/3);
	TextureText::texturetexts[TextureText::ylable2]->loadFromRenderedText(s2.str());
	std::stringstream sx2;
	sx2 << (energyEnd-energyBegin)/3 + energyBegin;
	TextureText::texturetexts[TextureText::xlable2]->loadFromRenderedText(sx2.str());
	std::stringstream sx3;
	sx3 << ((energyEnd-energyBegin)*2)/3 + energyBegin;
	TextureText::texturetexts[TextureText::xlable3]->loadFromRenderedText(sx3.str());
	std::stringstream sx4;
	sx4 << energyEnd;
	TextureText::texturetexts[TextureText::xlable4]->loadFromRenderedText(sx4.str());



}
