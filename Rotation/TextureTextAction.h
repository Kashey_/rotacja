/*
 * TextureTextAction.h
 *
 *  Created on: 04 ���. 2014 �.
 *      Author: Vladimir
 */

#ifndef TEXTURETEXTACTION_H_
#define TEXTURETEXTACTION_H_
#include "TextureText.h"
#include "ActionFocus.h"

class TextureTextAction: public TextureText, public ActionFocus{
public:
	TextureTextAction(Window::windowenum w, font tf, SDL_Color color = {255, 255, 255, 255}, std::string tt="");
};
#endif /* TEXTURETEXTACTION_H_ */
