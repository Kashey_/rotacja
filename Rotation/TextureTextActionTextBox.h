/*
 * TextureTextActionTextBox.h
 *
 *  Created on: 04 ���. 2014 �.
 *      Author: Vladimir
 */

#ifndef TEXTURETEXTACTIONTEXTBOX_H_
#define TEXTURETEXTACTIONTEXTBOX_H_
#include <SDL.h>
#include "TextureTextAction.h"
#include "ImageSpector.h"

class TextureTextActionTextBox:public TextureTextAction{
public:
	TextureTextActionTextBox(Window::windowenum w, font ft, SDL_Color color = {255, 255, 255, 255}, std::string tt="");
	void doAction(int i);
	void setKey(SDL_Event* e);
	static void openfile(const char* filedir);
private:
	std::string text2;
};
#endif /* TEXTURETEXTACTIONTEXTBOX_H_ */
