/*
 * Reseter.cpp
 *
 *  Created on: 03 ���. 2014 �.
 *      Author: Vladimir
 */

#include "Reseter.h"

#include "Crystall.h"
#include "ImageSpector.h"

Reseter::Reseter(TextureTextActionCountTyped* c1, TextureTextActionCountTyped* c2)
: count1(c1), count2(c2){
	// TODO Auto-generated constructor stub

}

void Reseter::doAction(int i){
	Crystall::crystall->reset();
	ImageSpector::imageSpector->reset();
	count1->reset();
	count2->reset();
}
