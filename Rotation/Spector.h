/*
 * Spector.h
 *
 *  Created on: 31 ���. 2014 �.
 *      Author: Vladimir
 */

#ifndef SPECTOR_H_
#define SPECTOR_H_
#include <SDL.h>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>

class Spector {
public:
	Spector();
	virtual ~Spector();
	double** intensity;
	int line;
	int column;
	double scaleE;
	double scaleI;
	double* energy;
	void render(int wWin, int hWin, SDL_Renderer* r, int shiftColor, double energyBegin);
	void resetColor(SDL_Renderer* r, unsigned short i);
	bool readFile(std::string path, double& energyBegin, double& energyEnd, int& l);
	void recalculateMinMax(double energyS);
	void setScaleI(double d);
};

#endif /* SPECTOR_H_ */
