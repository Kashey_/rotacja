/*
 * Texture.h
 *
 *  Created on: 04 ���. 2014 �.
 *      Author: Vladimir
 */

#ifndef TEXTURE_H_
#define TEXTURE_H_
#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_image.h>
#include <string>
#include <stdio.h>
#include "Window.h"
class Texture{
public:
	enum texturegrafic{pointerLeftUp, pointerLeftDown, pointerRightUp, pointerRightDown, redbuttonUp, redbuttonDown, backgroundCount, backgroundTextBox, printButtonUp, printButtonDown, x10Up, x10Down, x_yUp, x_yDown, resetUp, resetDown, numberOfTextureGraficss};
	static Texture* textureGrafics[numberOfTextureGraficss];
	static void close();
	Texture(Window::windowenum);
	virtual ~Texture();
	void free();
	void renderIt(int x, int y, SDL_Rect* clip = NULL, double angle = 0.0, SDL_Point* center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);
	int getWidth();
	int getHeight();
	Window::windowenum getTWindow();
	void setColor(Uint8 red, Uint8 green, Uint8 blue);
	void setMeasurements(int a, int b);
	static std::string correctText(std::string text, int length, int fntSize);
	static void loadTexture(Texture*t, std::string path);
	static void loadTextureFromText(Texture* t, std::string text, TTF_Font* font, SDL_Color color);
	static Texture* createTexture(Window::windowenum w, std::string);
	static Texture* createTextureFromText(Window::windowenum w, std::string text, TTF_Font* font, SDL_Color color);
	static void renderTetureGrafics();
protected:
	SDL_Texture* texture;
	int width;
	int height;
	Window::windowenum tWindow;
};

#endif /* TEXTURE_H_ */
