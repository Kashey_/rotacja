/*
 * TextureTextActionCountTyped.cpp
 *
 *  Created on: 04 ���. 2014 �.
 *      Author: Vladimir
 */

#include "TextureTextActionCountTyped.h"

#include "defs.h"
#include "SwitchIcons.h"
#include "Window.h"

class SwitchIcons;

TextureTextActionCountTyped::TextureTextActionCountTyped(typeCount n, Window::windowenum w, font tf, SDL_Color color, float f, std::string tt)
	:TextureTextActionCount(w, tf, color, f, tt){
	this->number = n;
}
void TextureTextActionCountTyped::doAction(int i){
	switch(i){
	case leftButton:
		if(SwitchIcons::instance.getValue()){
			actionCristall(-10);
			count-=10;
		}else{
			actionCristall(-1);
			count-=1;
		}
		break;
	case rightButton:
		if(SwitchIcons::instance.getValue()){
			actionCristall(10);
			count+=10;
		}else{
			actionCristall(1);
			count+=1;
		}
	}
	TextureText::upgradeTexturef(count);
}
void TextureTextActionCountTyped::reset(){
	count = 0;
	TextureText::upgradeTexturef(count);
}
void TextureTextActionCountTyped::actionCristall(int i){
	switch (this->number){
	case A:
		Crystall::crystall->rotate(0.0f, (float)i, 0.0f);
		break;
	case B:
		Crystall::crystall->rotate(i, 0, 0);
	}
}
