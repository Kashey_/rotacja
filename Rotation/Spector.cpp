/*
 * Spector.cpp
 *
 *  Created on: 31 ���. 2014 �.
 *      Author: Vladimir
 */

#include "Spector.h"
#include <algorithm>

Spector::Spector(): intensity(0), line(0), column(0), scaleE(0), scaleI(0), energy(0) {
}
Spector::~Spector() {}

void Spector::recalculateMinMax(double energyS){
	scaleE = energyS;
	double maxI = intensity[0][0];
	double minI = intensity[0][0];
	for(int i=1; i<line; ++i){
		for(int j = 0; j<column; ++j){
			if(maxI < intensity[i][j]) maxI = intensity[i][j];
			if(minI > intensity[i][j]) minI = intensity[i][j];
		}
	}
	scaleI = maxI;
}
void Spector::render(int wWin, int hWin, SDL_Renderer* r, int shiftColor, double energyBegin){
	for(int i = 1; i < line; ++i){
		for(int j = 0; j < column; ++j){
			resetColor(r, j + shiftColor);
			int ii = i-1;
			SDL_RenderDrawLine(r, (energy[ii] - energyBegin) * wWin / scaleE, hWin - ((intensity[ii][j]) * hWin / scaleI), (energy[i] - energyBegin) * wWin / scaleE, hWin - ((intensity[i][j]) * hWin / scaleI));
			//SDL_RenderDrawPoint(r, (energy[i] - energy[0]) * wWin / scaleE, hWin - ((intensity[i][j]) * hWin / scaleI));
		}
	}
}
void Spector::setScaleI(double d){
	this->scaleI = d;
}
bool Spector::readFile(std::string path, double& energyBegin, double& energyEnd, int& l){
	std::ifstream file(path.c_str());
	if(file ==0) return false;
	std::string str;
	std::getline(file, str);
	std::istringstream is (str);
	column=0;
	line=1;
	auto pred = [](char ch) { return !isspace(ch); };
	while(is>>str)	   ++column;
	while(file){
		if(!getline(file, str)) break;
		if(std::find_if(str.begin(), str.end(), pred) == str.end()) continue;
		++line;
	}
	if(column == 0)
		return false;
	energy = new double[line];
	--column;
	intensity = new double*[line];
	for(int i=0;i<line;++i){
		intensity[i] = new double[column];
	}
	file.clear();
	file.seekg(0, std::ios::beg);
	for(int i=0;i<line;++i){
		file>>energy[i];
		for(int j=0;j<column;++j)
			file>>intensity[i][j];
	}
	file.close();
	l = line;
	if(energy[0] < energyBegin) energyBegin = energy[0];
	if(energy[line-1] > energyEnd) energyEnd = energy[line-1];
	return true;
}
void Spector::resetColor(SDL_Renderer* r, unsigned short i){
	switch(i){
	case 0:
		SDL_SetRenderDrawColor(r, 128, 0, 0, 255);// Maroon
		break;
	case 1:
		SDL_SetRenderDrawColor(r, 0, 100, 0, 255);// Green forest
		break;
	case 2:
		SDL_SetRenderDrawColor(r, 255, 0, 0, 255);// Red
		break;
	case 3:
		SDL_SetRenderDrawColor(r, 0, 255, 0, 255);// Green
		break;
	case 4:
		SDL_SetRenderDrawColor(r, 0, 0, 255, 255);// Blue
		break;
	case 5:
		SDL_SetRenderDrawColor(r, 255, 255, 0, 255);// Yellow
		break;
	case 6:
		SDL_SetRenderDrawColor(r, 0, 255, 255, 255);// Cyan (aqua)
		break;
	case 7:
		SDL_SetRenderDrawColor(r, 255, 0, 255, 255);// Magenta
		break;
	case 8:
		SDL_SetRenderDrawColor(r, 0, 0, 128, 255);// Navy
		break;
	case 9:
		SDL_SetRenderDrawColor(r, 128, 0, 128, 255);// Purple
		break;
	case 10:
		SDL_SetRenderDrawColor(r, 128, 128, 0, 255);// Olive
		break;
	case 11:
		SDL_SetRenderDrawColor(r, 0, 128, 128, 255);// Teal
		break;
	case 12:
		SDL_SetRenderDrawColor(r, 128, 128, 128, 255);// Grey
		break;
	case 13:
		SDL_SetRenderDrawColor(r, 192, 192, 192, 255);// Silver
		break;
	case 14:
		SDL_SetRenderDrawColor(r, 127, 255, 212, 255);// Aquamarine
		break;
	case 15:
		SDL_SetRenderDrawColor(r, 255, 228, 196, 255);// Bisque
		break;
	case 16:
		SDL_SetRenderDrawColor(r, 138, 43, 226, 255);// Bluviolet
		break;
	case 17:
		SDL_SetRenderDrawColor(r, 165, 42, 42, 255);// Brown
		break;
	case 18:
		SDL_SetRenderDrawColor(r, 222, 184, 135, 255);// Burlywood
		break;
	default:
		SDL_SetRenderDrawColor(r, 255, 255, 255, 255);// White
	}
}
