/*
 * SwitchIcons.cpp
 *
 *  Created on: 03 ���. 2014 �.
 *      Author: Vladimir
 */

#include "SwitchIcons.h"


SwitchIcons SwitchIcons::instance = 0;

SwitchIcons::SwitchIcons(Button* b) {
	button = b;
	value = false;
}
bool SwitchIcons::getValue(){
	return this->value;
}
void SwitchIcons::doAction(int i){
	this->button->switchTexture();
	this->value = !value;
}
