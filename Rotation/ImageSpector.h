/*
 * ImageSpector.h
 *
 *  Created on: 28 ���. 2014 �.
 *      Author: Vladimir
 */

#ifndef IMAGESPECTOR_H_
#define IMAGESPECTOR_H_

//#include <cmath>
#include <string>
#include <vector>
#include "ActionFocus.h"
#include "Crystall.h"
#include "Quaterion.h"
#include "Spector.h"
#include "Window.h"

class ImageSpector: public ActionFocus {
public:
	ImageSpector(Window::windowenum);
	virtual ~ImageSpector();
	void doAction(int i){};
	void setKey(SDL_Event* e){};
	void readFile(std::string path);
	void render();
	void resetColor(unsigned short i);
	void rotateSpector(float ak, float ab, float ac);
	void recalculate();
	void print();
	void reset();
	double Max(double a, double b);
	double Min(double a, double b);
	double energyBegin;
	double energyEnd;
	int line;
	std::vector<Spector> spector;
	Quaterion::point vecB;
	Quaterion::point vecC;
	Quaterion::point vecK;
	Window::windowenum swindow;
	float anglek, angleb, anglec;
	static ImageSpector* imageSpector;
};

#endif /* IMAGESPECTOR_H_ */
