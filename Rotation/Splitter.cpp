/*
 * Splitter.cpp
 *
 *  Created on: 04 ���. 2014 �.
 *      Author: Vladimir
 */

#include "Splitter.h"

Splitter::Splitter(Action** ac, unsigned short c){
	this->actions = ac;
	this->count = c;
}
void Splitter::doAction(int i){
	for(int j=0; j<count;++j){
		actions[j]->doAction(i);
	}
}
void Splitter::setKey(SDL_Event* e){
	for(int j=0; j<count;++j){
		actions[j]->setKey(e);
	}
}
void Splitter::setFocus(bool b){
	for(int j=0; j<count;++j){
		actions[j]->setFocus(b);
	}
}

