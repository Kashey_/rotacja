/*
 * TextureTextActionCountTyped.h
 *
 *  Created on: 04 ���. 2014 �.
 *      Author: Vladimir
 */

#ifndef TEXTURETEXTACTIONCOUNTTYPED_H_
#define TEXTURETEXTACTIONCOUNTTYPED_H_

#include <SDL_pixels.h>
#include <string>
#include "TextureTextActionCount.h"
#include "Crystall.h"

class TextureTextActionCountTyped: public TextureTextActionCount{
public:
	enum typeCount{A, B};
	TextureTextActionCountTyped(typeCount n, Window::windowenum w, font tf, SDL_Color color = {255, 255, 255, 255}, float f=0.0, std::string tt="");
	void doAction(int i);
	void actionCristall(int i);
	void reset();
	typeCount number;
};
#endif /* TEXTURETEXTACTIONCOUNTTYPED_H_ */
