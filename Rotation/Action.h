/*
 * Action.h
 *
 *  Created on: 30 ���� 2014 �.
 *      Author: Vladimir
 */

#ifndef ACTION_H_
#define ACTION_H_

union SDL_Event;

class Action{
public:
		virtual ~Action() = default;
		virtual void doAction(int i) = 0;
		virtual void setKey(SDL_Event* e) = 0;
		virtual void setFocus(bool b)=0;
	};

#endif /* ACTION_H_ */
