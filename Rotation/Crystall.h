/*
 * Crystall.h
 *
 *  Created on: 04 ���. 2014 �.
 *      Author: Vladimir
 */

#ifndef CRYSTALL_H_
#define CRYSTALL_H_

#include <utility>
#include <SDL_rect.h>
#include "ActionFocus.h"
#include "defs.h"
#include "Window.h"
#include "Quaterion.h"
#include "ImageSpector.h"

class Crystall: public ActionFocus{
	struct point2{
		SDL_Point p; int s;
	};
	const int CENTER_H = SCREEN_DIRECTION_HEIGHT/2 - 30;
public:
	Crystall(Window::windowenum w);
	virtual ~Crystall();
	void doAction(int i){};
	void setKey(SDL_Event* e){};
	void drawAxis();
	void drawCrystall();
	void rotate(float anglek, float angleb=0, float anglec=0);
	void reset();
	static Crystall* crystall;
private:
	//void Quaterion2point(Quaterion::point* p, Quaterion::Quaterion* q, Quaterion::point* output);
	void drawlineFromCenter(int x, int y, int i, int j=0);
	void drawline(SDL_Point p1, SDL_Point p2, int i, int j=0);
	void drawCrystallLine(Quaterion::point p1, Quaterion::point p2, double maxy, double miny);
	void drawCrystallLineSegment(Crystall::point2* p, int count);
	void rotateCrystall(Quaterion* rotation, Quaterion* q, int count);
	template <typename T>
	void sortPairArray(std::pair<int, T>* tmpy, int n);
	SDL_Point adduction(Quaterion::point p);
	double Max(double a, double b);
	double Min(double a, double b);
	Quaterion::point crystallpoints[8];
	Quaterion::point axispoints[4];
	Window::windowenum cwindow;
};

#endif /* CRYSTALL_H_ */
