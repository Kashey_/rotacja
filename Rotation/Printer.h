/*
 * Printer.h
 *
 *  Created on: 01 ���. 2014 �.
 *      Author: Vladimir
 */

#ifndef PRINTER_H_
#define PRINTER_H_

#include "Action.h"

class Printer: public Action{
public:
	Printer();
	virtual ~Printer() = default;
	virtual void doAction(int i);
	virtual void setKey(SDL_Event* e){};
	virtual void setFocus(bool b){};
};

#endif /* PRINTER_H_ */
