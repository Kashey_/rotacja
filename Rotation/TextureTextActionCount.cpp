/*
 * TextureTextActionCount.cpp
 *
 *  Created on: 04 ���. 2014 �.
 *      Author: Vladimir
 */

#include "TextureTextActionCount.h"

TextureTextActionCount::TextureTextActionCount(Window::windowenum w, font tf, SDL_Color color, float f, std::string tt):
TextureTextAction(w, tf, color, tt){
	this->count = f;
}
void TextureTextActionCount::doAction(int i){
	switch(i){
	case leftButton:
		count-=1;
		break;
	case rightButton:
		count+=1;
	}
	TextureText::upgradeTexturef(count);
}
void TextureTextActionCount::setKey(SDL_Event* e){};

