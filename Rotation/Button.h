/*
 * Button.h
 *
 *  Created on: 04 ���. 2014 �.
 *      Author: Vladimir
 */

#ifndef BUTTON_H_
#define BUTTON_H_
#include <SDL_rect.h>
#include "Action.h"
#include "defs.h"
#include "Windows.h"
#include "Texture.h"

class Button {
public:
	enum LButtonSprite{	BUTTON_SPRITE_MOUSE_OUT, BUTTON_SPRITE_MOUSE_OVER_MOTION, BUTTON_SPRITE_MOUSE_DOWN, BUTTON_SPRITE_MOUSE_UP, BUTTON_SPRITE_MOUSE_TOTAL };
	static Button* buttons[numberOfButtons];
	Button(buttonType b, int x=0, int y=0, Action* a=0, Texture::texturegrafic t = Texture::pointerLeftUp);
	virtual ~Button();
	void setPosition(int x, int y);
	void handleEvent(SDL_Event* e);
	void render();
	void setTexture(Texture::texturegrafic up, Texture::texturegrafic down);
	int getTexture();
	SDL_Point* getPosition();
	void setActionObject(Action* a);
	void switchTexture();
private:
	Action* actionObject;
	SDL_Point position;
	Texture::texturegrafic texture[BUTTON_SPRITE_MOUSE_TOTAL];
	LButtonSprite mCurrentSprite;
	buttonType type;
};

#endif /* BUTTON_H_ */
