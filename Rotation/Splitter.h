/*
 * Splitter.h
 *
 *  Created on: 04 ���. 2014 �.
 *      Author: Vladimir
 */

#ifndef SPLITTER_H_
#define SPLITTER_H_
#include <SDL.h>
#include "Action.h"

class Splitter: public Action{
public:
	virtual ~Splitter() = default;
	Splitter(Action** ac, unsigned short c);
	void doAction(int i);
	void setKey(SDL_Event* e);
	void setFocus(bool b = true);
private:
	Action** actions;
	unsigned short count;
};

#endif /* SPLITTER_H_ */
