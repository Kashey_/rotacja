/*
 * Window.cpp
 *
 *  Created on: 07 ���. 2014 �.
 *      Author: Vladimir
 */

#include "Window.h"

#include <SDL_rect.h>

#include "Button.h"
#include "Texture.h"
#include "TextureText.h"

Window* Window::windows[numberOfWindows];
Window::Window() {
	this->window = 0;
	this->renderers[0] = 0;
	this->keyboardfocus = false;
	this->mouseFocus = false;
	this->fullScreen = false;
	this->shown = false;
	this->windowID = -1;
	this->width = 0;
	this->height = 0;
	this->minimized = 0;
	this->focusElement = 0;
}

Window::~Window() {
	// TODO Auto-generated destructor stub
}
SDL_Renderer* Window::getRenderer(renderer i){
	return this->renderers[i];
}
SDL_Window* Window::getSDLWindow(){
	return this->window;
}

unsigned Window::getWindowId(){
	return this->windowID;
}
bool Window::createWindow(std::string title, int xpos, int ypos, int w, int h){
	this->window = SDL_CreateWindow(title.c_str(), xpos, ypos, w, h, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
	if(window == 0){ printf("Window could not be created. SDL_Error %s\n", SDL_GetError());return false;}
	this->mouseFocus = true;
	this->keyboardfocus = true;
	this->width = w;
	this->height = h;
	this->renderers[normal] = SDL_CreateRenderer(this->window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if(this->renderers[normal] == 0){printf("renderer could not be created. SDL Error %s\n", SDL_GetError());SDL_DestroyWindow(this->window); window = 0; return false;}
	SDL_SetRenderDrawColor(this->renderers[normal], 255, 255, 255, 255);
	this->windowID = SDL_GetWindowID(this->window);
	shown = true;
	return true;
}
void upgrateWindowElements(unsigned w){
#define _1st Window::windows[Window::direction]->getHeight() - Texture::textureGrafics[Texture::backgroundTextBox]->getHeight()
#define _2nd _1st - Texture::textureGrafics[Texture::pointerLeftUp]->getWidth()
#define _3rd _2nd - Texture::textureGrafics[Texture::pointerLeftUp]->getWidth()
#define _4rd _3rd - Texture::textureGrafics[Texture::pointerLeftUp]->getWidth()
	if(w == Window::windows[Window::direction]->getWindowId()){
		Texture::textureGrafics[Texture::backgroundCount]->setMeasurements(Window::windows[Window::direction]->getWidth() - 2 * Texture::textureGrafics[Texture::pointerLeftUp]->getWidth(), Texture::textureGrafics[Texture::pointerLeftUp]->getHeight());
		Texture::textureGrafics[Button::buttons[textBoxbutton]->getTexture()]->setMeasurements(Window::windows[Window::direction]->getWidth() - Texture::textureGrafics[Texture::pointerLeftUp]->getWidth(), Texture::textureGrafics[Texture::pointerLeftUp]->getHeight());

		Button::buttons[buttonLeftB]->setPosition(0, _2nd);
		Button::buttons[buttonRightB]->setPosition(Window::windows[Window::direction]->getWidth()-Texture::textureGrafics[Button::buttons[buttonLeftB]->getTexture()]->getWidth(), _2nd);
		Button::buttons[buttonLeftA]->setPosition(Button::buttons[buttonLeftB]->getPosition()->x, _3rd);
		Button::buttons[buttonRightA]->setPosition(Button::buttons[buttonRightB]->getPosition()->x, _3rd);
		Button::buttons[textBoxbutton]->setPosition(0, _1st);
		Button::buttons[redbutton]->setPosition(Button::buttons[buttonRightB]->getPosition()->x, _1st);
		Button::buttons[printbutton]->setPosition(0, _4rd);
		Button::buttons[x10Button]->setPosition(Texture::textureGrafics[Button::buttons[printbutton]->getTexture()]->getWidth(), _4rd);
		Button::buttons[x_yButton]->setPosition(Texture::textureGrafics[Button::buttons[printbutton]->getTexture()]->getWidth() + Texture::textureGrafics[Button::buttons[x10Button]->getTexture()]->getWidth(), _4rd);
		Button::buttons[resetButton]->setPosition(Window::windows[Window::direction]->getWidth() - Texture::textureGrafics[Button::buttons[resetButton]->getTexture()]->getWidth(), _4rd);
		//Texture::renderTetureGrafics();
		//TextureText::renderTetureTexts();
		//SDL_RenderPresent(Window::windows[w]->getRenderer(Window::normal));
	}else if(w == Window::windows[Window::mainform]->getWindowId()){

	}
#undef _1st
#undef _2nd
#undef _3rd
#undef _4rd
}
void Window::handleEvent(SDL_Event& e){
	if(e.type == SDL_WINDOWEVENT && e.window.windowID == this->windowID){
		//bool updateCaption;
		switch(e.window.event){
		case SDL_WINDOWEVENT_SHOWN:
			this->shown = true;		break;
		case SDL_WINDOWEVENT_HIDDEN:
			this->shown = false;	break;
		case SDL_WINDOWEVENT_SIZE_CHANGED:
			this->width = e.window.data1;
			this->height = e.window.data2;
			upgrateWindowElements(this->windowID);
			//SDL_RenderPresent(this->renderers[normal]);
			break;
		case SDL_WINDOWEVENT_EXPOSED:
			//SDL_RenderPresent(this->renderers[normal]);
		case SDL_WINDOWEVENT_ENTER:
			//this->mouseFocus = true;
			//updateCaption = true;
			break;
		case SDL_WINDOWEVENT_LEAVE:
			//this->mouseFocus = false;
			//updateCaption = true;
			break;
		case SDL_WINDOWEVENT_FOCUS_GAINED:
			//this->keyboardfocus = true;
			//updateCaption = true;
			break;
		case SDL_WINDOWEVENT_FOCUS_LOST:
			//this->keyboardfocus = false;
			//updateCaption = true;
			break;
		case SDL_WINDOWEVENT_MINIMIZED:
			this->minimized = true;	break;
		case SDL_WINDOWEVENT_MAXIMIZED:
			this->minimized = false;break;
		case SDL_WINDOWEVENT_RESTORED:
			this->minimized = false;break;
		case SDL_WINDOWEVENT_CLOSE:
			SDL_HideWindow(window);
			break;
		}
		//if(updateCaption){
		//	std::stringstream caption;
		//	caption << "SDL tut - ID: "<< this->windowID <<" Mouse Focus "<<((mouseFocus)?"on":"off") <<" kbFocus "<<((keyboardfocus)?"on":"off");
		//	SDL_SetWindowTitle(window, caption.str().c_str());
		//}
	}
}
void Window::focus(){
	if(!this->shown) SDL_ShowWindow(this->window);
	SDL_RaiseWindow(this->window);
}
void Window::render(){
	if(this->minimized)		return;
	//SDL_SetRenderDrawColor(this->renderers[normal], 255, 255, 255, 255);
	//SDL_RenderClear(this->renderers[normal]);
	//SDL_RenderPresent(this->renderers[normal]);
}
void Window::free(){
	for(int i=0; i<numberOfRenderer; ++i){if(this->renderers != 0) SDL_DestroyRenderer(this->renderers[i]); this->renderers[i]=NULL;}
	if(this->window != 0)SDL_DestroyWindow(this->window);
	this->mouseFocus = false;
	this->keyboardfocus = false;
	this->width = 0;
	this->height = 0;
}
bool Window::init(){
	windows[mainform] = new Window();
	if(!windows[mainform]->createWindow("Mainform", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT))
		{printf("Window %s could not be created. SDL_Errer %s.\n", "mainform", SDL_GetError());	return false;}
	windows[direction] = new Window();
	if(!windows[direction]->createWindow("Direction", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_UNDEFINED, SCREEN_DIRECTION_WIDTH, SCREEN_DIRECTION_HEIGHT))
		{printf("Window %s could not be created. SDL_Errer %s.\n", "direction", SDL_GetError());	return false;}
	return true;
}
void Window::close(){
	for(int i=0; i<numberOfWindows; ++i){windows[i]->free(); windows[i]=NULL;}
}
int Window::getWidth(){
	return this->width;
}
int Window::getHeight(){
	return this->height;
}
bool Window::hasMouseFocus(){
	return this->mouseFocus;
}
bool Window::hasKeyboardFocus(){
	return this->keyboardfocus;
}
bool Window::isMinimized(){
	return this->minimized;
}
bool Window::isShown(){
	return this->shown;
}
