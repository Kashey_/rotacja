/*
 * Quaterion.h
 *
 *  Created on: 28 ���. 2014 �.
 *      Author: Vladimir
 */

#ifndef QUATERION_H_
#define QUATERION_H_
#include <cmath>
#include <SDL_stdinc.h>

class Quaterion {
public:
	struct point{
		double x, y, z;
		void normalize();
	};
	Quaterion(point p);
	Quaterion(point p, double iw);
	Quaterion();
	void multiplication(Quaterion* q1);
	virtual ~Quaterion() = default;
	void normalize();
	void toPoint(point& p);
	double projected(double mp_a, double mp_b, double mp_c);
	double x;
	double y;
	double z;
	double w;
};

#endif /* QUATERION_H_ */
