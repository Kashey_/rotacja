/*
 * TextureTextActionCount.h
 *
 *  Created on: 04 ���. 2014 �.
 *      Author: Vladimir
 */

#ifndef TEXTURETEXTACTIONCOUNT_H_
#define TEXTURETEXTACTIONCOUNT_H_
#include <SDL_pixels.h>
#include <string>
#include "TextureTextAction.h"

class TextureTextActionCount:public TextureTextAction{
public:
	TextureTextActionCount(Window::windowenum w, font tf, SDL_Color color = {255, 255, 255, 255}, float f=0.0, std::string tt="");
	void doAction(int i);
	void setKey(SDL_Event* e);
	float count;
};

#endif /* TEXTURETEXTACTIONCOUNT_H_ */
