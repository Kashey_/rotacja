/*
 * main.cpp
 *
 *  Created on: 21 ���� 2014 �.
 *      Author: Vladimir
 */

#include <stdio.h>
#include <SDL.h>
#include <SDL_error.h>
#include <SDL_events.h>
#include <SDL_hints.h>
#include <SDL_image.h>
#include <SDL_keyboard.h>
#include <SDL_main.h>
#include <SDL_rect.h>
#include <SDL_render.h>
#include <SDL_stdinc.h>
#include <SDL_ttf.h>
#include <SDL_video.h>
#include <string>

#include "Button.h"
#include "Crystall.h"
#include "defs.h"
#include "Texture.h"
#include "TextureText.h"
#include "TextureTextActionCountTyped.h"
#include "TextureTextActionTextBox.h"
#include "Window.h"
#include "ImageSpector.h"
#include"Printer.h"
#include "SwitchIcons.h"
#include "Reseter.h"

#define _1st Window::windows[Window::direction]->getHeight() - Texture::textureGrafics[Texture::backgroundTextBox]->getHeight()
#define _2nd _1st - Texture::textureGrafics[Texture::pointerLeftUp]->getWidth()
#define _3rd _2nd - Texture::textureGrafics[Texture::pointerLeftUp]->getWidth()
#define _4rd _3rd - Texture::textureGrafics[Texture::pointerLeftUp]->getWidth()

void close(){
	Window::close();
	TextureText::close();
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}
bool init(){
	if(SDL_Init(SDL_INIT_VIDEO)<0) {printf("SDL could not initialize. SDL Error %s\n", SDL_GetError());	return false;}
	if(!(IMG_Init(IMG_INIT_PNG)&IMG_INIT_PNG)){printf("SDL_image could not initialize. SDL_image Error %s.\n", IMG_GetError()); return false;}
	if(TTF_Init()==-1){printf("SDL_ttf could not initialize. SDL_ttf Error %s.\n", TTF_GetError()); return false;}
	if(!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) printf("Warning, Linear texture filtering not enabled.\n");
	return Window::init();
}
void loadMedia(){
	TextureText::loadFonts();
	Texture::textureGrafics[Texture::pointerLeftUp] = Texture::createTexture(Window::direction, "textures/pointerLeftUp.png");
	Texture::textureGrafics[Texture::pointerLeftDown] = Texture::createTexture(Window::direction, "textures/pointerLeftDown.png");
	Texture::textureGrafics[Texture::pointerRightUp] = Texture::createTexture(Window::direction, "textures/pointerRightUp.png");
	Texture::textureGrafics[Texture::pointerRightDown] = Texture::createTexture(Window::direction, "textures/pointerRightDown.png");
	Texture::textureGrafics[Texture::backgroundCount] = Texture::createTexture(Window::direction, "textures/backgroundCount.png");
	Texture::textureGrafics[Texture::backgroundTextBox] = Texture::createTexture(Window::direction, "textures/backgroudTextBoxt.png");
	Texture::textureGrafics[Texture::redbuttonUp] = Texture::createTexture(Window::direction, "textures/redbuttonUp.png");
	Texture::textureGrafics[Texture::redbuttonDown] = Texture::createTexture(Window::direction, "textures/redbuttonDown.png");
	Texture::textureGrafics[Texture::printButtonUp] = Texture::createTexture(Window::direction, "textures/pup.png");
	Texture::textureGrafics[Texture::printButtonDown] = Texture::createTexture(Window::direction, "textures/pdown.png");
	Texture::textureGrafics[Texture::x10Up] = Texture::createTexture(Window::direction, "textures/x10up.png");
	Texture::textureGrafics[Texture::x10Down] = Texture::createTexture(Window::direction, "textures/x10down.png");
	Texture::textureGrafics[Texture::x_yUp] = Texture::createTexture(Window::direction, "textures/x_yup.png");
	Texture::textureGrafics[Texture::x_yDown] = Texture::createTexture(Window::direction, "textures/x_ydown.png");
	Texture::textureGrafics[Texture::resetUp] = Texture::createTexture(Window::direction, "textures/rup.png");
	Texture::textureGrafics[Texture::resetDown] = Texture::createTexture(Window::direction, "textures/rdown.png");
	Button::buttons[buttonLeftB] = new Button(leftButton);
	Button::buttons[buttonRightB] = new Button(rightButton);
	Button::buttons[buttonLeftA] = new Button(leftButton);
	Button::buttons[buttonRightA] = new Button(rightButton);
	Button::buttons[textBoxbutton] = new Button(textBoxType);
	Button::buttons[redbutton] = new Button(red);
	Button::buttons[printbutton] = new Button(print);
	Button::buttons[x10Button] = new Button(x10);
	Button::buttons[x_yButton] = new Button(x_y);
	Button::buttons[resetButton] = new Button(reset);
	Button::buttons[printbutton]->setTexture(Texture::printButtonUp, Texture::printButtonDown);
	Button::buttons[x10Button]->setTexture(Texture::x10Up, Texture::x10Down);
	Button::buttons[x_yButton]->setTexture(Texture::x_yUp, Texture::x_yDown);
	Button::buttons[resetButton]->setTexture(Texture::resetUp, Texture::resetDown);
	Button::buttons[buttonLeftB]->setTexture(Texture::pointerLeftUp, Texture::pointerLeftDown);
	Button::buttons[buttonRightB]->setTexture(Texture::pointerRightUp, Texture::pointerRightDown);
	Button::buttons[buttonLeftA]->setTexture(Texture::pointerLeftUp, Texture::pointerLeftDown);
	Button::buttons[buttonRightA]->setTexture(Texture::pointerRightUp, Texture::pointerRightDown);
	Button::buttons[textBoxbutton]->setTexture(Texture::backgroundTextBox, Texture::backgroundTextBox);
	Button::buttons[redbutton]->setTexture(Texture::redbuttonUp, Texture::redbuttonDown);
	Button::buttons[printbutton]->setPosition(0, _4rd);
	Button::buttons[x10Button]->setPosition(Texture::textureGrafics[Button::buttons[printbutton]->getTexture()]->getWidth(), _4rd);
	Button::buttons[x_yButton]->setPosition(Texture::textureGrafics[Button::buttons[printbutton]->getTexture()]->getWidth() + Texture::textureGrafics[Button::buttons[x10Button]->getTexture()]->getWidth(), _4rd);
	Button::buttons[resetButton]->setPosition(Window::windows[Window::direction]->getWidth() - Texture::textureGrafics[Button::buttons[resetButton]->getTexture()]->getWidth(), _4rd);
	Button::buttons[buttonLeftB]->setPosition(0, _2nd);
	Button::buttons[buttonRightB]->setPosition(Window::windows[Window::direction]->getWidth()-Texture::textureGrafics[Button::buttons[buttonRightB]->getTexture()]->getWidth(), _2nd);
	Button::buttons[buttonLeftA]->setPosition(Button::buttons[buttonLeftB]->getPosition()->x, _3rd);
	Button::buttons[buttonRightA]->setPosition(Button::buttons[buttonRightB]->getPosition()->x, _3rd);
	Button::buttons[textBoxbutton]->setPosition(0, _1st);
	Button::buttons[redbutton]->setPosition(Texture::textureGrafics[Button::buttons[textBoxbutton]->getTexture()]->getWidth(), _1st);

	Printer* printer = new Printer();
	Button::buttons[printbutton]->setActionObject(printer);
	SwitchIcons::instance = SwitchIcons(Button::buttons[x10Button]);
	Button::buttons[x10Button]->setActionObject(&SwitchIcons::instance);
//	Button::buttons[x_yButton]->setTexture(Texture::x_yUp, Texture::x_yDown);

	TextureTextActionTextBox* ttacb = new TextureTextActionTextBox(Window::direction, TextureText::cambriab1, {255, 0, 0, 255} );
	TextureText::texturetexts[TextureText::textFileName] = ttacb;
	Button::buttons[textBoxbutton]->setActionObject(ttacb);
	Button::buttons[redbutton]->setActionObject(ttacb);
	TextureTextActionCountTyped* ttac;
	ttac = new TextureTextActionCountTyped(TextureTextActionCountTyped::A, Window::direction, TextureText::sofachromeRegular, {0, 255, 0, 255});
	TextureText::texturetexts[TextureText::textA] = ttac;
	Button::buttons[buttonLeftA]->setActionObject(ttac);
	Button::buttons[buttonRightA]->setActionObject(ttac);
	TextureTextActionCountTyped* ttac2 = new TextureTextActionCountTyped(TextureTextActionCountTyped::B, Window::direction, TextureText::sofachromeRegular, {0, 255, 0, 255});
	TextureText::texturetexts[TextureText::textB] = ttac2;
	Button::buttons[buttonLeftB]->setActionObject(ttac2);
	Button::buttons[buttonRightB]->setActionObject(ttac2);
	Reseter* r = new Reseter(ttac, ttac2);
	Button::buttons[resetButton]->setActionObject(r);
	std::string txt = Texture::correctText("0", SCREEN_DIRECTION_WIDTH - (2 * Texture::textureGrafics[Button::buttons[buttonLeftB]->getTexture()]->getWidth()), fontDeciSize);
	TextureText::texturetexts[TextureText::textA]->loadFromRenderedText(txt);
	txt = Texture::correctText("0", SCREEN_DIRECTION_WIDTH - (2 * 20), fontDeciSize);
	TextureText::texturetexts[TextureText::textB]->loadFromRenderedText(txt);
	TextureText* tx = new TextureText(Window::mainform, TextureText::cambriab1, {0, 255, 0, 0});
	TextureText::texturetexts[TextureText::ylable1] = tx;
	TextureText::texturetexts[TextureText::ylable1]->loadFromRenderedText("0");
	tx = new TextureText(Window::mainform, TextureText::cambriab1, {0, 255, 0, 0});
	TextureText::texturetexts[TextureText::ylable2] = tx;
	TextureText::texturetexts[TextureText::ylable2]->loadFromRenderedText("300");
	tx = new TextureText(Window::mainform, TextureText::cambriab1, {0, 255, 0, 0});
	TextureText::texturetexts[TextureText::ylable3] = tx;
	TextureText::texturetexts[TextureText::ylable3]->loadFromRenderedText("600");
	tx = new TextureText(Window::mainform, TextureText::cambriab1, {0, 255, 0, 0});
	TextureText::texturetexts[TextureText::ylable4] = tx;
	TextureText::texturetexts[TextureText::ylable4]->loadFromRenderedText("900");
	tx = new TextureText(Window::mainform, TextureText::cambriab1, {0, 255, 0, 0});
	TextureText::texturetexts[TextureText::xlable2] = tx;
	TextureText::texturetexts[TextureText::xlable2]->loadFromRenderedText("0.30");
	tx = new TextureText(Window::mainform, TextureText::cambriab1, {0, 255, 0, 0});
	TextureText::texturetexts[TextureText::xlable3] = tx;
	TextureText::texturetexts[TextureText::xlable3]->loadFromRenderedText("0.60");
	tx = new TextureText(Window::mainform, TextureText::cambriab1, {0, 255, 0, 0});
	TextureText::texturetexts[TextureText::xlable4] = tx;
	TextureText::texturetexts[TextureText::xlable4]->loadFromRenderedText("0.90");
}
int main(int argc, char* args[]){
	FreeConsole();
	if(!init()) return 0;
	loadMedia();
	Crystall::crystall = new Crystall(Window::direction);
	ImageSpector::imageSpector = new ImageSpector(Window::mainform);
	SDL_Event e;
	SDL_StartTextInput();
	while(true){
		while(SDL_PollEvent(&e)!=0){
			switch(e.type){
			case SDL_QUIT:
				close();
				return 0;
				break;
			case SDL_DROPFILE:{
				char* dropped_filedir = e.drop.file;
				TextureTextActionTextBox::openfile(dropped_filedir);
				SDL_free(dropped_filedir);
				break;
			}
			case SDL_KEYDOWN:
			case SDL_TEXTINPUT:
				if( focus != 0)	focus->setKey(&e);
				break;
			case SDL_WINDOWEVENT:
				if(e.window.event == SDL_WINDOWEVENT_CLOSE){
					close();
					return 0;
				}
				for(int i=0; i<Window::numberOfWindows;++i){
					Window::windows[i]->handleEvent(e);
				}
				break;
			default:
				for (int i=0; i<numberOfButtons; i++){
					Button::buttons[i]->handleEvent(&e);
				}
			}
		}

		SDL_SetRenderDrawColor(Window::windows[Window::direction]->getRenderer(Window::normal), 165, 160, 200, 0xFF);
		SDL_RenderClear(Window::windows[Window::direction]->getRenderer(Window::normal));
		SDL_SetRenderDrawColor(Window::windows[Window::mainform]->getRenderer(Window::normal), 0, 0, 0, 0xFF);
		SDL_RenderClear(Window::windows[Window::mainform]->getRenderer(Window::normal));
		for(int i=0; i<numberOfButtons; i++){
			Button::buttons[i]->render();
		}
		Texture::renderTetureGrafics();
		TextureText::renderTetureTexts();
		SDL_SetRenderDrawColor(Window::windows[Window::direction]->getRenderer(Window::normal), 44, 6, 111, 255);
		Crystall::crystall->drawAxis();
		Crystall::crystall->drawCrystall();
		ImageSpector::imageSpector->render();
		for(int i = 0; i < Window::numberOfWindows; ++i){
			Window::windows[i]->render();
		}
	SDL_RenderPresent(Window::windows[Window::direction]->getRenderer(Window::normal));
	SDL_RenderPresent(Window::windows[Window::mainform]->getRenderer(Window::normal));
	}
	close();
	return 0;
}
