/*
 * ActionFocus.h
 *
 *  Created on: 04 ���. 2014 �.
 *      Author: Vladimir
 */

#ifndef ACTIONFOCUS_H_
#define ACTIONFOCUS_H_
#include "Action.h"

class ActionFocus: public Action{
public:
	virtual ~ActionFocus() = default;
	void setFocus(bool b){
				this->focus = b;
			}
	private:
			bool focus;
};
#endif /* ACTIONFOCUS_H_ */
