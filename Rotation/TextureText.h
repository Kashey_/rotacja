/*
 * TextureText.h
 *
 *  Created on: 04 ���. 2014 �.
 *      Author: Vladimir
 */

#ifndef TEXTURETEXT_H_
#define TEXTURETEXT_H_

#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_image.h>
#include <string>
#include <stdio.h>
#include "Texture.h"
#include <iostream>
#include <sstream>
#include <SDL_ttf.h>
class TextureText: public Texture{
public:
	enum font{sofachromeRegular, cambriab1, numberOfFonts};
	enum texturetext{textA, textB, textFileName, ylable1, ylable2, ylable3, ylable4, xlable2, xlable3, xlable4, numberOfTextureTexts};
	static TextureText* texturetexts[numberOfTextureTexts];
	//static TTF_Font* TextureText::fonts[numberOfFonts];

	static void loadFonts();
	static void close();
	static void createTextureTexts();
	static TTF_Font* getFont(font f);

	TextureText(Window::windowenum w, font tf, SDL_Color color = {255, 255, 255, 255}, std::string tt="");
	bool loadFromRenderedText( std::string textureText);
	void setText(std::string s);
	std::string getText();
	void upgradeTexture();
	void upgradeTexturef(float f);
	void setColor(SDL_Color c);
	void setFont(font tf);
	void setColorAndFont(SDL_Color c, font tf);
	void upgradeTexture(std::string s);
	static void renderTetureTexts();
protected:
	std::string textureText;
	SDL_Color textcolor;
	font textfont;
private:
	static TTF_Font* fonts[numberOfFonts];
};

#endif /* TEXTURETEXT_H_ */
