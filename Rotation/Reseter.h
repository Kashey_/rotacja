/*
 * Reseter.h
 *
 *  Created on: 03 ���. 2014 �.
 *      Author: Vladimir
 */

#ifndef RESETER_H_
#define RESETER_H_

#include "Action.h"
#include "TextureTextActionCountTyped.h"

class Reseter : public Action{
public:
	Reseter(TextureTextActionCountTyped* c1, TextureTextActionCountTyped* c2);
	virtual void doAction(int i);
	virtual void setKey(SDL_Event* e){};
	virtual void setFocus(bool b){};
	TextureTextActionCountTyped* count1;
	TextureTextActionCountTyped* count2;

};

#endif /* RESETER_H_ */
