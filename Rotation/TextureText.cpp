/*
 * TextureText.cpp
 *
 *  Created on: 04 ���. 2014 �.
 *      Author: Vladimir
 */

#include "TextureText.h"
#define _1st Window::windows[Window::direction]->getHeight() - Texture::textureGrafics[Texture::backgroundTextBox]->getHeight()
#define _2nd _1st - Texture::textureGrafics[Texture::pointerLeftUp]->getWidth()
#define _3rd _2nd - Texture::textureGrafics[Texture::pointerLeftUp]->getWidth()

	TextureText* TextureText::texturetexts[numberOfTextureTexts];
	TTF_Font* TextureText::fonts[numberOfFonts];
TTF_Font* TextureText::getFont(font f){
	return fonts[f];
}
void TextureText::loadFonts(){
	fonts[sofachromeRegular] = TTF_OpenFont("fonts/sofachrome.regular.ttf", fontDeciSize);
	if(fonts[sofachromeRegular] == NULL)printf("Failed to load font. SDL_ttf Error %s.\n", TTF_GetError());
	fonts[cambriab1] = TTF_OpenFont("fonts/CAMBRIAB_1.TTF", fontDeciSize);
	if(fonts[cambriab1] == NULL)printf("Failed to load font. SDL_ttf Error %s.\n", TTF_GetError());
}
void TextureText::close(){
	for(int i=0; i<numberOfFonts;i++){TTF_CloseFont(fonts[i]);fonts[i]=NULL;}
	for(int i=0; i<numberOfTextureTexts;i++){texturetexts[i]->free(); texturetexts[i]=NULL;}
}
void TextureText::setColor(SDL_Color c){
	this->textcolor = c;
}
void TextureText::setFont(font tf){
	this->textfont = tf;
}
void TextureText::setColorAndFont(SDL_Color c, font tf){
	this->setColor(c);
	this->setFont(tf);
}
TextureText::TextureText(Window::windowenum w, font tf, SDL_Color color, std::string tt):Texture::Texture(w){
	textureText = tt;
	textcolor = color;
	textfont = tf;
}
void TextureText::setText(std::string s){
	this->textureText = s;
}
std::string TextureText::getText(){
	return this->textureText;
}
void TextureText::upgradeTexture(){
	if(!this->loadFromRenderedText(this->textureText))	printf( "Unable to upgrade text texture %s. SDL Error %s.\n", this->textureText.c_str(), SDL_GetError());
}
void TextureText::upgradeTexture(std::string s){
	std::string txt = this->textureText;
	txt += "|";
	txt += s;
	if(!this->loadFromRenderedText(txt))	printf( "Unable to upgrade text texture %s. SDL Error %s.\n", txt.c_str(), SDL_GetError());
}
void TextureText::upgradeTexturef(float f){
	std::stringstream txt (std::stringstream::in | std::stringstream::out);
	txt << this->textureText;
	txt <<f;
	if(!this->loadFromRenderedText(txt.str()))	printf( "Unable to upgrade text texture %s with number %f. SDL Error %s.\n", this->textureText.c_str(), f, SDL_GetError());
}
bool TextureText::loadFromRenderedText(std::string text){
	free();
	//SDL_Surface* txtSurface = TTF_RenderText_Solid(fonts[this->textfont], text.c_str(), this->textcolor);
	SDL_Surface* txtSurface = TTF_RenderUTF8_Solid(fonts[this->textfont], text.c_str(), this->textcolor);
	if(txtSurface == NULL){
		printf("*Unable to render text surface %s. SDL_ttf Error %s.\n", text.c_str(), TTF_GetError());return false; }
	texture = SDL_CreateTextureFromSurface(Window::windows[this->tWindow]->getRenderer(Window::normal), txtSurface);
	if(texture == NULL){printf("Unable to create texture from rendered text. SDL Error %s.\n", SDL_GetError()); return false;}
	width = txtSurface->w;
	height = txtSurface->h;
	return true;
}
void TextureText::renderTetureTexts(){
	int widthText;
	widthText = Window::windows[Window::direction]->getWidth() - Texture::textureGrafics[Texture::pointerLeftUp]->getWidth();
	if(TextureText::texturetexts[TextureText::textFileName]->getWidth() > widthText){
		SDL_Rect rect = {TextureText::texturetexts[TextureText::textFileName]->getWidth() - widthText, 0, widthText, TextureText::texturetexts[TextureText::textFileName]->getHeight()};
		TextureText::texturetexts[TextureText::textFileName]->renderIt(0, _1st, &rect);
	}else
		TextureText::texturetexts[TextureText::textFileName]->renderIt(0, _1st);
	widthText = Window::windows[Window::direction]->getWidth() - 2 * Texture::textureGrafics[Texture::pointerLeftUp]->getWidth();
	if(TextureText::texturetexts[TextureText::textA]->getWidth() > widthText){
		SDL_Rect recttextA = {TextureText::texturetexts[TextureText::textA]->getWidth() - widthText, 0, widthText, TextureText::texturetexts[TextureText::textA]->getHeight()};
		TextureText::texturetexts[TextureText::textA]->renderIt(Window::windows[TextureText::texturetexts[TextureText::textA]->getTWindow()]->getWidth(), _3rd - 3, &recttextA);
	}else
		TextureText::texturetexts[TextureText::textA]->renderIt(Window::windows[TextureText::texturetexts[TextureText::textA]->getTWindow()]->getWidth()/2-TextureText::texturetexts[TextureText::textA]->getWidth()/2, _3rd - 3);
	if(TextureText::texturetexts[TextureText::textB]->getWidth() > widthText){
		SDL_Rect recttextB = {TextureText::texturetexts[TextureText::textB]->getWidth() - widthText, 0, widthText, TextureText::texturetexts[TextureText::textB]->getHeight()};
		TextureText::texturetexts[TextureText::textB]->renderIt(Texture::textureGrafics[Texture::pointerLeftUp]->getWidth(), _2nd - 3, &recttextB);
	}else
		TextureText::texturetexts[TextureText::textB]->renderIt(Window::windows[TextureText::texturetexts[TextureText::textA]->getTWindow()]->getWidth()/2-TextureText::texturetexts[TextureText::textB]->getWidth()/2, _2nd - 3);

}
