/*
 * Quaterion.cpp
 *
 *  Created on: 28 ���. 2014 �.
 *      Author: Vladimir
 */

#include "Quaterion.h"

//#include <math.h>
//#include <cstdlib>
//#include <utility>

void Quaterion::point::normalize(){
	double temp = sqrt(pow(this->x, 2) + pow(this->y, 2) + pow(this->z, 2));
	this->x /= temp;
	this->y /= temp;
	this->z /= temp;
}
Quaterion::Quaterion(point p):
x(p.x), y(p.y), z(p.z), w(0)  {
}
Quaterion::Quaterion(point p, double angle){
	p.normalize();
	angle *= M_PI;
	angle /= 180;
	this->x = p.x * sin(angle/2);
	this->y = p.y * sin(angle/2);
	this->z = p.z * sin(angle/2);
	this->w = cos(angle/2);
}
Quaterion::Quaterion():x(0), y(0), z(0), w(0){}
void Quaterion::normalize(){
	double temp = sqrt(pow(this->x, 2) + pow(this->y, 2) + pow(this->z, 2) + pow(this->w, 2));
	this->x /= temp;
	this->y /= temp;
	this->z /= temp;
	this->w /= temp;
}
void Quaterion::toPoint(point& p){
	p.x = x;
	p.y = y;
	p.z = z;
}
void Quaterion::multiplication(Quaterion* q2){
	Quaterion* temp = new Quaterion();
	Quaterion* q1 = this;
	temp->x = q1->w * q2->x + q1->x * q2->w + q1->y * q2->z - q1->z * q2->y ;
	temp->y = q1->w * q2->y - q1->x * q2->z + q1->y * q2->w + q1->z * q2->x ;
	temp->z = q1->w * q2->z + q1->x * q2->y - q1->y * q2->x + q1->z * q2->w ;
	temp->w = q1->w * q2->w - q1->x * q2->x - q1->y * q2->y - q1->z * q2->z ;
	q2->x = -q2->x;
	q2->y = -q2->y;
	q2->z = -q2->z;
	q1->x = q2->w * temp->x + q2->x * temp->w + q2->y * temp->z - q2->z * temp->y;
	q1->y = q2->w * temp->y - q2->x * temp->z + q2->y * temp->w + q2->z * temp->x;
	q1->z = q2->w * temp->z + q2->x * temp->y - q2->y * temp->x + q2->z * temp->w;
	q1->w = q2->w * temp->w - q2->x * temp->x - q2->y * temp->y - q2->z * temp->z;
	q2->x = -q2->x;
	q2->y = -q2->y;
	q2->z = -q2->z;
	delete temp;
}
double Quaterion::projected(double mp_a, double mp_b, double mp_c){
	point l1 = {0, mp_b, 0};
	point l2 = {0, 0, mp_c};
	point l3 = {mp_a, 0, 0};
	point tmp1, tmp2, tmp3;
	tmp1.x=(l1.x * this->x + l1.y * this->y + l1.z * this->z) / (pow(this->x, 2) + pow(this->y, 2) + pow(this->z, 2))*this->x;
	tmp1.y=(l1.x * this->x + l1.y * this->y + l1.z * this->z) / (pow(this->x, 2) + pow(this->y, 2) + pow(this->z, 2))*this->y;
	tmp1.z=(l1.x * this->x + l1.y * this->y + l1.z * this->z) / (pow(this->x, 2) + pow(this->y, 2) + pow(this->z, 2))*this->z;

	tmp2.x=(l2.x * this->x + l2.y * this->y + l2.z * this->z) / (pow(this->x, 2) + pow(this->y, 2) + pow(this->z, 2))*this->x;
	tmp2.y=(l2.x * this->x + l2.y * this->y + l2.z * this->z) / (pow(this->x, 2) + pow(this->y, 2) + pow(this->z, 2))*this->y;
	tmp2.z=(l2.x * this->x + l2.y * this->y + l2.z * this->z) / (pow(this->x, 2) + pow(this->y, 2) + pow(this->z, 2))*this->z;

	tmp3.x=(l3.x * this->x + l3.y * this->y + l3.z * this->z) / (pow(this->x, 2) + pow(this->y, 2) + pow(this->z, 2))*this->x;
	tmp3.y=(l3.x * this->x + l3.y * this->y + l3.z * this->z) / (pow(this->x, 2) + pow(this->y, 2) + pow(this->z, 2))*this->y;
	tmp3.z=(l3.x * this->x + l3.y * this->y + l3.z * this->z) / (pow(this->x, 2) + pow(this->y, 2) + pow(this->z, 2))*this->z;

	double intens=(pow((tmp2.x + tmp3.x), 2) +pow((tmp2.y+tmp3.y), 2)+ pow((tmp2.z+tmp3.z), 2))+(pow(tmp1.x, 2)+pow(tmp1.y, 2)+pow(tmp1.z, 2));
	return intens;
}
