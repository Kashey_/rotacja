/*
 * SwitchIcons.h
 *
 *  Created on: 03 ���. 2014 �.
 *      Author: Vladimir
 */

#ifndef SWITCHICONS_H_
#define SWITCHICONS_H_

#include "Action.h"
#include "Button.h"

class SwitchIcons : public Action{
public:
	SwitchIcons(Button* b);
	virtual void doAction(int i);
	virtual void setKey(SDL_Event* e){};
	virtual void setFocus(bool b){};
	bool getValue();
	static SwitchIcons instance;
private:
	Button* button;
	bool value;
};

#endif /* SWITCHICONS_H_ */
