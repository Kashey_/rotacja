/*
 * Window.h
 *
 *  Created on: 07 ���. 2014 �.
 *      Author: Vladimir
 */

#ifndef WINDOW_H_
#define WINDOW_H_
union SDL_Event;
struct SDL_Renderer;
struct SDL_Window;
#include <SDL_video.h>
#include <SDL_error.h>
#include <SDL_events.h>
#include <SDL_render.h>
#include <cstdio>
#include <sstream>
#include <string>

#include "defs.h"

class Window {
public:
	enum renderer{normal, numberOfRenderer};
	enum windowenum{mainform, direction, numberOfWindows};
	Window();
	virtual ~Window();
	bool createWindow(std::string title="", int xpos=SDL_WINDOWPOS_UNDEFINED, int ypos=SDL_WINDOWPOS_UNDEFINED, int w=SCREEN_WIDTH, int h=SCREEN_HEIGHT);
	void handleEvent(SDL_Event& e);
	void focus();
	void render();
	void free();
	int getWidth();
	int getHeight();
	unsigned getWindowId();
	bool hasMouseFocus();
	bool hasKeyboardFocus();
	bool isMinimized();
	bool isShown();
	SDL_Window* getSDLWindow();
	SDL_Renderer* getRenderer(renderer);
	static bool init();
	static void close();
	static Window* windows[numberOfWindows];
private:
	SDL_Renderer* renderers[numberOfRenderer];
	SDL_Window* window;
	//R::renderer wRenderer;
	unsigned windowID;
	int width;
	int height;
	bool mouseFocus;
	bool keyboardfocus;
	bool fullScreen;
	bool minimized;
	bool shown;
	Action* focusElement;
};

#endif /* WINDOW_H_ */
