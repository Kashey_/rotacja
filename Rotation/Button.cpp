/*
 * Button.cpp
 *
 *  Created on: 04 ���. 2014 �.
 *      Author: Vladimir
 */

#include "Button.h"
Button* Button::buttons[numberOfButtons];
Button::Button(buttonType b, int x, int y, Action* a, Texture::texturegrafic t) {
	actionObject = a;
	position.x=x;
	position.y=y;
	mCurrentSprite = BUTTON_SPRITE_MOUSE_OUT;
	for(int i=0; i<BUTTON_SPRITE_MOUSE_TOTAL;++i){
		texture[i]=Texture::pointerLeftUp;
	}
	texture[BUTTON_SPRITE_MOUSE_OUT]=t;
	this->type = b;
}

Button::~Button() {
}
void Button::setPosition(int x, int y){
	position.x = x;
	position.y = y;
}
void Button::setActionObject(Action* a){
	this->actionObject = a;
}
void Button::setTexture(Texture::texturegrafic up, Texture::texturegrafic down){
	this->texture[BUTTON_SPRITE_MOUSE_UP] = up;
	this->texture[BUTTON_SPRITE_MOUSE_OUT] = up;
	this->texture[BUTTON_SPRITE_MOUSE_DOWN] = down;
}
SDL_Point* Button::getPosition(){
	return &position;
}
int Button::getTexture(){
	return this->texture[mCurrentSprite];
}
void Button::handleEvent(SDL_Event* e){
	if(e->type == SDL_MOUSEBUTTONDOWN || e->type == SDL_MOUSEBUTTONUP){
		int x, y;
		SDL_GetMouseState(&x, &y);
		if(x > position.x && x < position.x + Texture::textureGrafics[this->texture[BUTTON_SPRITE_MOUSE_UP]]->getWidth() && y > position.y
				&& y < position.y + Texture::textureGrafics[this->texture[BUTTON_SPRITE_MOUSE_UP]]->getHeight()){
			switch(e->type){
			case SDL_MOUSEBUTTONDOWN:
				mCurrentSprite = BUTTON_SPRITE_MOUSE_DOWN;
				if(focus != 0) focus->setFocus(false);
				focus = this->actionObject;
				actionObject->doAction(this->type);
				break;
			case SDL_MOUSEBUTTONUP:
				mCurrentSprite = BUTTON_SPRITE_MOUSE_UP;
				break;
			}
		}
	}
}
void Button::switchTexture(){
	Texture::texturegrafic t;
	t = this->texture[BUTTON_SPRITE_MOUSE_UP];
	this->texture[BUTTON_SPRITE_MOUSE_UP] = this->texture[BUTTON_SPRITE_MOUSE_DOWN];
	this->texture[BUTTON_SPRITE_MOUSE_DOWN] = t;
}
void Button::render(){
	Texture::textureGrafics[this->texture[mCurrentSprite]]->renderIt(position.x, position.y);
}
