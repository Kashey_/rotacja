/*
 * Texture.cpp
 *
 *  Created on: 30 ���� 2014 �.
 *      Author: Vladimir
 */

#include "Texture.h"
#define _1st Window::windows[Window::direction]->getHeight() - Texture::textureGrafics[Texture::backgroundTextBox]->getHeight()
#define _2nd _1st - Texture::textureGrafics[Texture::pointerLeftUp]->getWidth()
#define _3rd _2nd - Texture::textureGrafics[Texture::pointerLeftUp]->getWidth()

Texture* Texture::textureGrafics[Texture::numberOfTextureGraficss];
Texture* Texture::createTexture(Window::windowenum w, std::string path){
	Texture* t = new Texture(w);
	Texture::loadTexture(t, path);
	return t;
}
Texture* Texture::createTextureFromText(Window::windowenum w, std::string path, TTF_Font* font, SDL_Color color){
	Texture* t = new Texture(w);
	Texture::loadTextureFromText(t, path, font, color);
	return t;
}
void Texture::close(){
	for(int i=0; i<numberOfTextureGraficss;i++){ Texture::textureGrafics[i]->free(); Texture::textureGrafics[i]=NULL;}
}
void Texture::loadTextureFromText(Texture* t, std::string text, TTF_Font* font, SDL_Color color){
	t->free();
	if(text.length() == 0) return;
	SDL_Surface* txtSurface = TTF_RenderText_Solid(font, text.c_str(), color);
	if(txtSurface == NULL){printf("Unable to render text surface. SDL_ttf Error %s.\n", TTF_GetError());}
	else{
		t->texture = SDL_CreateTextureFromSurface(Window::windows[t->tWindow]->getRenderer(Window::normal), txtSurface);
		if(t->texture == NULL){printf("Unable to create texture from rendered text. SDL Error %s.\n", SDL_GetError());}
		t->width = txtSurface->w;
		t->height = txtSurface->h;
	}
}
void Texture::loadTexture(Texture* t, std::string path){
	t->free();
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if(loadedSurface == NULL) printf("Unable to load image %s, SDL_image Error %s.\n", path.c_str(), IMG_GetError());
	else{
		t->texture = SDL_CreateTextureFromSurface(Window::windows[t->tWindow]->getRenderer(Window::normal), loadedSurface);
		t->setMeasurements(loadedSurface->w, loadedSurface->h);
		SDL_FreeSurface(loadedSurface);
		if(t->texture == NULL){printf("Unable to create texture from %s, SDL_image Error %s.\n", path.c_str(), IMG_GetError());}
	}
}
Texture::Texture(Window::windowenum w) {
	texture = 0;
	width = 0;
	height = 0;
	this->tWindow = w;
}
Texture::~Texture() {
	free();
}
void Texture::setMeasurements(int a, int b){
	this->width = a;
	this->height = b;
}
void Texture::free(){
if(texture != NULL){
	SDL_DestroyTexture(texture);
	texture=NULL;
	width=0;
	height=0;
}}
void Texture::renderIt(int x, int y, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip){
	SDL_Rect renderQuad = {x, y, width, height};
	if(clip != NULL){
		renderQuad.w = clip->w;
		renderQuad.h=clip->h;
	}
	SDL_RenderCopyEx(Window::windows[this->tWindow]->getRenderer(Window::normal), texture, clip, &renderQuad, angle, center, flip);
}
int Texture::getWidth(){return width;}

int Texture::getHeight(){return height;}

Window::windowenum Texture::getTWindow(){
	return this->tWindow;
}

std::string Texture::correctText(std::string text, int length, int fontSize){
	unsigned num = (length/fontSize)+2;
	if(text.size()>num) text = text.substr(text.size() - num);
	return text;
}
void Texture::setColor(Uint8 red, Uint8 green, Uint8 blue){
	SDL_SetTextureColorMod(texture, red, green, blue);
}
void Texture::renderTetureGrafics(){
	//textureGrafics[backgroundTextBox]->renderIt(0, _1st);
	Texture::textureGrafics[Texture::backgroundCount]->renderIt(Texture::textureGrafics[Texture::pointerLeftUp]->getWidth(), _2nd);
	Texture::textureGrafics[Texture::backgroundCount]->renderIt(Texture::textureGrafics[Texture::pointerLeftUp]->getWidth(), _3rd);
}

