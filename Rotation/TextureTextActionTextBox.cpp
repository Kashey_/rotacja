/*
 * TextureTextActionTextBox.cpp
 *
 *  Created on: 04 ���. 2014 �.
 *      Author: Vladimir
 */

#include "TextureTextActionTextBox.h"
#include <iostream>

TextureTextActionTextBox::TextureTextActionTextBox(Window::windowenum w, font ft, SDL_Color color, std::string tt):TextureTextAction(w, ft, color, tt){
	this->text2="";
}
void TextureTextActionTextBox::setKey(SDL_Event* e){
	if(e->type == SDL_TEXTINPUT){
		if(!( (e->text.text[0] == 'c' || e->text.text[0] == 'C') && (e->text.text[0] == 'v' || e->text.text[0] == 'V') && (SDL_GetModState() & KMOD_CTRL))){
			this->textureText +=e->text.text[1];
			this->textureText +=e->text.text[0];
			this->upgradeTexture(this->text2);
		}
	}else{
		switch(e->key.keysym.sym){
		case SDLK_BACKSPACE:
			if(this->textureText.length() > 0){
				textureText.resize(textureText.length()-1);
				this->upgradeTexture(this->text2);
			}
			break;
		case SDLK_c:
			if(SDL_GetModState() & KMOD_CTRL){
				std::string s = this->textureText;
				s += this->text2;
				SDL_SetClipboardText(s.c_str());
				this->upgradeTexture(this->text2);
			}
			break;
		case SDLK_v:
			if(SDL_GetModState() & KMOD_CTRL){
				this->textureText += SDL_GetClipboardText();
				this->upgradeTexture(this->text2);
			}
			break;
		}
	}
}
void TextureTextActionTextBox::doAction(int i){
	if(i == red){
		std::string s = this->textureText;
		s += this->text2;
		openfile(s.c_str());
	}
	this->upgradeTexture(this->text2);
}
void TextureTextActionTextBox::openfile(const char* filedir){
	ImageSpector::imageSpector->readFile(filedir);
//		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, "File dropped on window", filedir, windows[direction]);
}
